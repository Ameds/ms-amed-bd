package mx.com.axkansoluciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsAmedBdApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsAmedBdApplication.class, args);
	}

}
