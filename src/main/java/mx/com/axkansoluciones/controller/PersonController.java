package mx.com.axkansoluciones.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.axkansoluciones.model.Person;
import mx.com.axkansoluciones.repository.PersonDao;

@RestController
@RequestMapping("persons")
public class PersonController {
	
	@Autowired
	private PersonDao personDao;
	
	
	@GetMapping
	public ResponseEntity<List<Person>> getPerson(){
		List<Person> persons = personDao.findAll();
		return ResponseEntity.ok(persons);
	}
	
	
	@PostMapping
	public ResponseEntity<Person> createPerson(@RequestBody Person person){
		Person newPerson =personDao.save(person);
		return ResponseEntity.ok(newPerson);
	}
	
	@PutMapping
	public ResponseEntity<Person> updatePerson(@RequestBody Person person){
		Optional<Person> optionalPerson = personDao.findById(person.getId_person());
		if(optionalPerson.isPresent()) {
			Person existPerson = person;
			
			existPerson.setNamee(person.getNamee());
			existPerson.setLastnameP(person.getLastnameP());
			existPerson.setLastnameM(person.getLastnameM());
			existPerson.setGender(person.getGender());
			existPerson.setFec_nac(person.getFec_nac());
			existPerson.setCurp(person.getCurp());
			existPerson.setEst_civ(person.getEst_civ());
			existPerson.setRfc(person.getRfc());
			existPerson.setNss(person.getNss());
			
			personDao.save(existPerson);
			
			return new ResponseEntity<Person>(existPerson, HttpStatus.OK);	
		}	
		throw new RuntimeException("La persona ingresada no existe");
	}
	
	
	
	@DeleteMapping(value = "/{personID}")
	public ResponseEntity<Void> delete(@PathVariable("personID") Long personId){
		Person perso = personDao.findById(personId)
				.orElseThrow(() -> new RuntimeException("La persona ingresada no existe"));
		personDao.delete(perso);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
