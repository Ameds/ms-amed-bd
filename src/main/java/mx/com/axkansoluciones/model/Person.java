package mx.com.axkansoluciones.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

	@NoArgsConstructor
	@AllArgsConstructor
	@Getter
	@Setter
	@Builder
	@Entity
	@Table(name="person")
public class Person {
		
		@Id
		@Column(name="id_person")
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id_person;
		
		@Column(name = "namee")
		private String namee;
		
		@Column(name = "lastnameP")
		private String lastnameP;
		
		@Column(name = "lastnameM")
		private String lastnameM;
		
		@Column(name = "gender")
		private String gender;
		
		@Column(name = "fec_nac")
		private String fec_nac;
		
		@Column(name = "curp")
		private String curp;
		
		@Column(name = "est_civ")
		private String est_civ;
		
		@Column(name = "rfc")
		private String rfc;
		
		@Column(name = "nss")
		private double nss;
}
