package mx.com.axkansoluciones.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.axkansoluciones.model.Person;

@Repository
public interface PersonDao extends JpaRepository<Person, Long> 
{
	
}
